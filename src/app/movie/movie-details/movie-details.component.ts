import { Component, OnInit, Input } from '@angular/core';
import { MovieDetails } from './../movie.model';

@Component({
  selector: 'movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {

  @Input() details: MovieDetails;

  constructor() { }

  ngOnInit(): void {
  }

}
