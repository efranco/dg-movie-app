import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Movie, MovieDetails } from './movie.model';

@Component({
  selector: 'app-movies',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  movieList: Array<MovieDetails> = [];
  filteredList: Array<MovieDetails> = [];
  yearList: Array<number> = [];
  yearSelected: string;
  searchTerm:string;

  constructor(
    private services: AppService
  ) { }

  ngOnInit(): void {
    this.getMovies('batman');
  }

  getMovies(title){
    this.services.getMovieResults(title).subscribe((results:any) =>{
      if(results.Search){
        results.Search.map((movie) => {
          this.checkYearRange(movie.Year);
          this.getMovieDetails(movie.imdbID);
        });
      }
    })
  }

  getMovieDetails(imdbID){
    this.services.getMovieDetails(imdbID).subscribe((results:any) =>{
      let movie = new MovieDetails(results);
      this.movieList.push(movie);
      this.createListCopy();
      this.sortByYear();
    });
  }

  checkYearRange(year){
    let list = String(year).split('–');
    list.forEach( year => {
      this.getListByDecade(year);
    })
  }

  getListByDecade(year){
    let decade = Math.floor(year / 10) * 10;
    this.yearList.push(decade);
    this.yearList = Array.from(new Set(this.yearList));
    this.yearList.sort((a, b) => b - a);
  }

  toggleSelected(year){
    if(this.yearSelected == year){
      this.resetFilterList();
    }else{
      this.filterByDecade(year);
    }
  }

  filterByDecade(year){
    this.resetFilterList();
    this.yearSelected = year;
    let minYear = Number(year);
    let maxYear = Number(year) + 10;
    this.filteredList = Object.assign([], this.filteredList)
      .filter((movie) => {
        return (movie.year >= minYear && movie.year < maxYear)
      });
  }

  resetFilterList(){
    this.yearSelected = '';
    this.searchTerm = '';
    this.createListCopy();
    this.sortByYear();
  }

  createListCopy(){
    this.filteredList = Object.assign([], this.movieList);
  }

  sortByYear(){
    this.filteredList.sort((a, b) => {
      let yearA = String(a.year).split('–')[0];
      let yearB = String(b.year).split('–')[0];
      return Number(yearB) - Number(yearA)}
    );
  }

  search(){
    if(!this.searchTerm){
      this.createListCopy();
    }
    this.filteredList = Object.assign([], this.filteredList)
      .filter((movie) => movie.title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1)
  }

}
