export class Movie {
  poster: string;
  title: string;
  year: string;
  imdbId: string;

  constructor(movie){
    this.poster = movie.Poster;
    this.title =  movie.Title;
    this.year = movie.Year;
    this.imdbId = movie.imdbID;
  }
}

export class MovieDetails extends Movie {
  plot:string;
  rated: string;
  runtime: string;
  released: string;

  constructor(moviedetails){
    super(moviedetails);
    this.plot = moviedetails.Plot;
    this.rated = moviedetails.Rated;
    this.runtime = moviedetails.Runtime;
    this.released = moviedetails.Released;
  }
}
