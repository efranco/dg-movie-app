import { Injectable } from '@angular/core';
import { HttpClient, HttpParams,  } from '@angular/common/http';
import { map, catchError, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  urlPath = 'http://www.omdbapi.com';
  key = 'b66952b';

  constructor(private http:HttpClient) { }

  private setSearchParams(title){
    let params = new HttpParams();
    params = params.append('apikey', this.key);
    params = params.append('s', title);
    return params;
  }

  private setMovieIdParams(id){
    let params = new HttpParams();
    params = params.append('apikey', this.key);
    params = params.append('i', id);
    return params;
  }

  getMovieResults(title){
    let searchParam = this.setSearchParams(title);
    return this.http.get(this.urlPath, {params: searchParam});
  }

  getMovieDetails(imdbID){
    let movieParams = this.setMovieIdParams(imdbID);
    return this.http.get(this.urlPath, {params: movieParams});
  }
}
